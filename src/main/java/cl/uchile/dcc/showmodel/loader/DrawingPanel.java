/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.showmodel.loader;

import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.awt.Color.BLACK;
import static java.awt.Color.WHITE;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author hmoraga
 */
public class DrawingPanel extends JPanel {
    private List<List<Point2D>> listaObjetos;
    private final Dimension marco;

    /**
     * 
     * @param marco dimensiones gráficas
     */
    public DrawingPanel(Dimension marco) {
        this.marco = marco;
        super.setSize(marco);
    }

    /**
     *
     * @return
     */
    public List<List<Point2D>> getListaObjetos() {
        return this.listaObjetos;
    }

    /**
     *
     * @param listaObjetos
     */
    public void setListaObjetos(List<List<Point2D>> listaObjetos) {
        this.listaObjetos = listaObjetos;
    }

    /**
     *
     * @return
     */
    public Dimension getMarco(){
        return this.marco;
    }
    
    /**
     *
     */
    @Override
    public void repaint() {
        super.repaint();
    }

    /**
     *
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(BLACK);

        //inicializo el dibujo
        super.setForeground(WHITE);

        for (int i = 0; i < this.listaObjetos.size(); i++) {
            List<Point2D> objeto = this.listaObjetos.get(i);
            
            for (int j = 0; j < objeto.size(); j++) {
                Point2D p = objeto.get(j);
                Point2D q = objeto.get((j+1)% objeto.size());
                
                g.drawLine((int)p.getX(), (int)p.getY(), (int)q.getX(), (int)q.getY());
            }
        }
        super.setVisible(true);
    }
}
