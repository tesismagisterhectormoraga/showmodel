/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.showmodel.loader;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class ListObjects {
    private List<List<Point2D>> listaObjetos; // lista original de objetos
    private List<Pair<Double>> listaVelocidades; // velocidades de cada objeto

    /**
     *
     * @param listaObjetos
     * @param listaVelocidades
     */
    public ListObjects(List<List<Point2D>> listaObjetos, List<Pair<Double>> listaVelocidades) {
        this.listaObjetos = listaObjetos;
        this.listaVelocidades = listaVelocidades;
    }

    /**
     *
     * @param time es el tiempo absoluto
     * @return lista de poligonos actualizada
     */
    public List<List<Point2D>> getUpdatedObjects(double time) {
        // creo una copia local de los objetos
        List<List<Point2D>> listaTemporalObjetos = new ArrayList<>(this.listaObjetos.size());

        for (int i = 0; i < this.listaObjetos.size(); i++) {
            List<Point2D> obj = this.listaObjetos.get(i);
            List<Point2D> objTemp = new ArrayList<>(obj.size());
            Pair<Double> velocidadObjeto = this.listaVelocidades.get(i);

            obj.forEach((p) -> {
                objTemp.add(new Point2D(p));
            });
                
            objTemp.forEach((p) -> {
                double deltax = velocidadObjeto.getFirst()*time;
                double deltay = velocidadObjeto.getSecond()*time;
                
                p.add(deltax, deltay);
            });
            
            listaTemporalObjetos.add(objTemp);
        }

        return listaTemporalObjetos;
    }

    /**
     *
     * @return
     */
    public List<List<Point2D>> getListaObjetos() {
        return this.listaObjetos;
    }

    /**
     *
     * @param listaObjetos
     */
    public void setListaObjetos(List<List<Point2D>> listaObjetos) {
        this.listaObjetos = listaObjetos;
    }

    /**
     *
     * @return
     */
    public List<Pair<Double>> getListaVelocidades() {
        return this.listaVelocidades;
    }

    /**
     *
     * @param listaVelocidades
     */
    public void setListaVelocidades(List<Pair<Double>> listaVelocidades) {
        this.listaVelocidades = listaVelocidades;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        for (List<Point2D> obj : this.listaObjetos) {
            for (Point2D p : obj) {
                hash = 37 * hash + p.hashCode();
            }
        }
        
        for (Pair<Double> veloc : this.listaVelocidades) {
            hash = 37 * hash + veloc.hashCode();
        }
        
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ListObjects other = (ListObjects) obj;
        if (!Objects.equals(this.listaObjetos, other.listaObjetos)) {
            return false;
        }
        return Objects.equals(this.listaVelocidades, other.listaVelocidades);
    }
}
