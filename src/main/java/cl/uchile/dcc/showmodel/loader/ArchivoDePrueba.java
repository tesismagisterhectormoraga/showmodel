/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.System.out;

/**
 * Este archivo de prueba me genera un rombo y 4 triangulos distribuidos a lo
 * largo de la pantalla. El rombo se encuentra centrado en la pantalla y los 4
 * triangulos en cada uno de los bordes apuntando hacia el centro.
 *
 * @author hmoraga
 */
public class ArchivoDePrueba {
    private static final String FILENAME = "C:\\Users\\hmoraga\\Documents\\NetBeansProjects\\ShowModel\\prueba.txt";

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))) {
            bw.write("marco:((-10.0,10.0),(-10.0,10.0))");
            bw.newLine();
            bw.write("timeIntervals:0.0-1.0");
            bw.newLine();
            bw.write("framesPerSecond:100");
            bw.newLine();
            bw.write("0.0 0.0");
            bw.newLine();
            bw.write("4 2.0 0.0 0.0 2.0 -2.0 0.0 0.0 -2.0");
            bw.newLine();
            bw.write("-9.0 0.0");
            bw.newLine();
            bw.write("3 9.0 0.0 10.0 -1.0 10.0 1.0");
            bw.newLine();
            bw.write("9.0 0.0");
            bw.newLine();
            bw.write("3 -9.0 0.0 -10.0 1.0 -10.0 -1.0");
            bw.newLine();
            bw.write("0.0 9.0");
            bw.newLine();
            bw.write("3 0.0 -9.0 -1.0 -10.0 1.0 -10.0");
            bw.newLine();
            bw.write("0.0 -9.0");
            bw.newLine();
            bw.write("3 0.0 9.0 1.0 10.0 -1.0 10.0");
            bw.newLine();
            out.println("Done");
        } catch (IOException e) {}
    }
}
