/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.showmodel.loader;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static java.util.regex.Pattern.compile;

/**
 *
 * @author hmoraga
 */
public class LoadModel {
    private List<List<Point2D>> listaObjetos;
    private List<Pair<Double>> listaVelocidades;
    private List<Pair<Double>> marco;
    private int framesPerSecond;
    private double tinicial, tfinal;

    /**
     *
     * @param listaLineas
     */
    public LoadModel(List<String> listaLineas) {
        String patternString;
        Pattern pattern;
        Matcher matcher;

        // linea 0
        patternString = "marco:\\(\\((-?\\d+?.\\d+),(-?\\d+?.\\d+)\\),\\((-?\\d+?.\\d+),(-?\\d+?.\\d+)\\)\\)";
        pattern = compile(patternString);
        matcher = pattern.matcher(listaLineas.get(0));

        if (matcher.find()) {
            double minX = parseDouble(matcher.group(1));
            double maxX = parseDouble(matcher.group(2));
            double minY = parseDouble(matcher.group(3));
            double maxY = parseDouble(matcher.group(4));

            this.marco = new ArrayList<>(2);
            this.marco.add(new PairDouble(minX, maxX));
            this.marco.add(new PairDouble(minY, maxY));
        } else {
            this.marco = new ArrayList<>(2);
            this.marco.add(new PairDouble(-10.0, 10.0));
            this.marco.add(new PairDouble(-10.0, 10.0));
        }
        matcher.reset();

        //linea 1
        patternString = "timeIntervals:(\\d+.?\\d+)-(\\d+.?\\d+)";
        pattern = compile(patternString);
        matcher = pattern.matcher(listaLineas.get(1));

        if (matcher.find()) {
            this.tinicial = parseDouble(matcher.group(1));
            this.tfinal = parseDouble(matcher.group(2));
        } else {
            this.tinicial = 0.0;
            this.tfinal = 1.0;
        }
        matcher.reset();

        //linea 2
        patternString = "framesPerSecond:(\\d+)";
        pattern = compile(patternString);
        matcher = pattern.matcher(listaLineas.get(2));

        if (matcher.find()) {
            this.framesPerSecond = parseInt(matcher.group(1));
        } else {
            this.framesPerSecond = 10;
        }
        matcher.reset();

        this.listaVelocidades = new ArrayList<>();
        this.listaObjetos = new ArrayList<>();

        for (int i = 3; i < listaLineas.size(); i++) {
            String line = listaLineas.get(i);

            // leo los objetos uno a uno
            // estará la linea de la velocidades primero
            // luego la de los puntos
            if (i % 2 == 1) {
                patternString = "(-?\\d+?.\\d+)\\s+(-?\\d+?.\\d+)";
                pattern = compile(patternString);
                matcher = pattern.matcher(line);
                double velocidadX = 0.0, velocidadY = 0.0;

                if (matcher.find()) {
                    velocidadX = parseDouble(matcher.group(1));
                    velocidadY = parseDouble(matcher.group(2));
                }
                this.listaVelocidades.add(new PairDouble(velocidadX, velocidadY));
            } else {
                String[] split = line.split("\\s+");
                int cantPuntos = parseInt(split[0]);
                List<Point2D> listaPuntosObj = new ArrayList<>(cantPuntos);

                for (int j = 1; j < split.length; j += 2) {
                    Point2D p = new Point2D(parseDouble(split[j]), parseDouble(split[j + 1]));
                    listaPuntosObj.add(p);
                }

                this.listaObjetos.add(listaPuntosObj);
            }
            matcher.reset();
        }
    }

    /**
     *
     * @return
     */
    public List<List<Point2D>> getListaObjetos() {
        return this.listaObjetos;
    }

    /**
     *
     * @param listaObjetos
     */
    public void setListaObjetos(List<List<Point2D>> listaObjetos) {
        this.listaObjetos = listaObjetos;
    }

    /**
     *
     * @return
     */
    public List<Pair<Double>> getListaVelocidades() {
        return this.listaVelocidades;
    }

    /**
     *
     * @param listaVelocidades
     */
    public void setListaVelocidades(List<Pair<Double>> listaVelocidades) {
        this.listaVelocidades = listaVelocidades;
    }

    /**
     *
     * @return
     */
    public List<Pair<Double>> getMarco() {
        return this.marco;
    }

    /**
     *
     * @param marco
     */
    public void setMarco(List<Pair<Double>> marco) {
        this.marco = marco;
    }

    /**
     *
     * @return
     */
    public int getFramesPerSecond() {
        return this.framesPerSecond;
    }

    /**
     *
     * @param framesPerSecond
     */
    public void setFramesPerSecond(int framesPerSecond) {
        this.framesPerSecond = framesPerSecond;
    }

    /**
     *
     * @return
     */
    public double getTinicial() {
        return this.tinicial;
    }

    /**
     *
     * @param tinicial
     */
    public void setTinicial(double tinicial) {
        this.tinicial = tinicial;
    }

    /**
     *
     * @return
     */
    public double getTfinal() {
        return this.tfinal;
    }

    /**
     *
     * @param tfinal
     */
    public void setTfinal(double tfinal) {
        this.tfinal = tfinal;
    }

    // TODO: REVISAR AQUI, por que el TIME no se usa

    /**
     *
     * @param time
     */
    public void updatePositions(double time) {
        for (int i = 0; i < this.listaObjetos.size(); i++) {
            for (int j = 0; j < this.listaObjetos.get(i).size(); j++) {
                this.listaObjetos.get(i).get(j).add(this.listaVelocidades.get(i).getFirst() * time, this.listaVelocidades.get(i).getSecond() * time);
            }
        }
    }
}
