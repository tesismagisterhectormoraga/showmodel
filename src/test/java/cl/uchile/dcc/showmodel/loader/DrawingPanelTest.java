/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.showmodel.loader;

import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.awt.Dimension;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class DrawingPanelTest {
    
    /**
     *
     */
    public DrawingPanelTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaObjetos method, of class DrawingPanel.
     */
    @Test
    public void testGetListaObjetos() {
        out.println("getListaObjetos");
        List<Point2D> listaPuntos = new ArrayList<>(4);
        listaPuntos.add(new Point2D());
        listaPuntos.add(new Point2D(5,-5));
        listaPuntos.add(new Point2D(0,5));
        listaPuntos.add(new Point2D(-5,-5));
        
        List<List<Point2D>> listaListaPuntos = new ArrayList<>(1);
        listaListaPuntos.add(listaPuntos);

        List<List<Point2D>> expResult = listaListaPuntos;
        
        Dimension marcoOriginal = new Dimension(600,600);
        
        DrawingPanel instance = new DrawingPanel(marcoOriginal);
        instance.setListaObjetos(listaListaPuntos);
        
        List<List<Point2D>> result = instance.getListaObjetos();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMarco method, of class DrawingPanel.
     */
    @Test
    public void testGetMarco() {
        out.println("getMarco");
        List<Point2D> listaPuntos = new ArrayList<>(4);
        listaPuntos.add(new Point2D());
        listaPuntos.add(new Point2D(5,-5));
        listaPuntos.add(new Point2D(0,5));
        listaPuntos.add(new Point2D(-5,-5));
        
        List<List<Point2D>> listaListaPuntos = new ArrayList<>(1);
        listaListaPuntos.add(listaPuntos);

        Dimension expResult = new Dimension(600,600);
                
        DrawingPanel instance = new DrawingPanel(new Dimension(600, 600));
        instance.setListaObjetos(listaListaPuntos);
        
        Dimension result = instance.getMarco();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaObjetos method, of class DrawingPanel.
     */
    @Test
    public void testSetListaObjetos() {
        out.println("setListaObjetos");
        List<Point2D> listaPuntos = new ArrayList<>(4);
        listaPuntos.add(new Point2D(1,2));
        listaPuntos.add(new Point2D(7,2));
        listaPuntos.add(new Point2D(9,6));
        listaPuntos.add(new Point2D(3,6));
        List<List<Point2D>> listaObjetos = new ArrayList<>();
        listaObjetos.add(listaPuntos);
        
        List<Point2D> expResult = new ArrayList<>(4);
        expResult.add(new Point2D(1,2));
        expResult.add(new Point2D(7,2));
        expResult.add(new Point2D(9,6));
        expResult.add(new Point2D(3,6));
        
        DrawingPanel instance = new DrawingPanel(new Dimension(600, 600));
        instance.setListaObjetos(listaObjetos);
        
        assertTrue(instance.getListaObjetos().size()==1);
        assertTrue(instance.getListaObjetos().get(0).size()==4);
        assertEquals(expResult, instance.getListaObjetos().get(0));
    }
}
