/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.showmodel.loader;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.System.out;
import static java.nio.file.Files.readAllLines;
import static java.nio.file.Paths.get;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

/**
 *
 * @author hmoraga
 */
public class LoadModelTest {

    /**
     *
     */
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    private File createdFile;

    /**
     *
     */
    public LoadModelTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        this.createdFile = this.folder.newFile("parDeObjetos.txt");
        try (FileWriter fw = new FileWriter(this.createdFile, true);
                BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write("marco:((-10.0,10.0),(-10.0,10.0))");
            bw.newLine();
            bw.write("timeIntervals:0.2-1.0");
            bw.newLine();
            bw.write("framesPerSecond:100");
            bw.newLine();
            bw.write("1.0 1.0");
            bw.newLine();
            bw.write("4 4.0 0.0 0.0 4.0 -4.0 0.0 0.0 -4.0");
            bw.newLine();
            bw.write("-1.0 -1.0");
            bw.newLine();
            bw.write("3 -8.0 -4.0 4.0 -4.0 4.0 4.0");
            bw.newLine();
        }
        out.println("");
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.createdFile.deleteOnExit();
    }

    /**
     * Test of getListaObjetos method, of class LoadModel.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testGetListaObjetos() throws IOException {
        out.println("getListaObjetos");
        LoadModel instance = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));
        List<List<Point2D>> expResult = new ArrayList<>();
        List<Point2D> obj1 = new ArrayList<>();
        obj1.add(new Point2D(4, 0));
        obj1.add(new Point2D(0, 4));
        obj1.add(new Point2D(-4, 0));
        obj1.add(new Point2D(0, -4));
        expResult.add(obj1);
        List<Point2D> obj2 = new ArrayList<>();
        obj2.add(new Point2D(-8, -4));
        obj2.add(new Point2D(4, -4));
        obj2.add(new Point2D(4, 4));
        expResult.add(obj2);

        List<List<Point2D>> result = instance.getListaObjetos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaObjetos method, of class LoadModel.
     * @throws java.io.IOException
     */
    @Test
    public void testSetListaObjetos() throws IOException {
        out.println("setListaObjetos");
        LoadModel instance = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));
        List<List<Point2D>> result = instance.getListaObjetos();
        
        List<List<Point2D>> expResult = new ArrayList<>();
        
        List<Point2D> obj1 = new ArrayList<>();
        obj1.add(new Point2D(0, -2));
        obj1.add(new Point2D(2, 4));
        obj1.add(new Point2D(-2, 4));
        
        List<Point2D> obj2 = new ArrayList<>();
        obj2.add(new Point2D(-2, -2));
        obj2.add(new Point2D(2, -2));
        obj2.add(new Point2D(0, 4));
        
        expResult.add(obj1);
        expResult.add(obj2);  
        
        instance.setListaObjetos(expResult);
        assertNotEquals(expResult, result);
    }

    /**
     * Test of getListaVelocidades method, of class LoadModel.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testGetListaVelocidades() throws IOException {
        out.println("getListaVelocidades");
        LoadModel instance = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));
        List<Pair<Double>> expResult = new ArrayList<>();
        expResult.add(new PairDouble(1.0, 1.0));
        expResult.add(new PairDouble(-1.0, -1.0));
        List<Pair<Double>> result = instance.getListaVelocidades();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaVelocidades method, of class LoadModel.
     * @throws java.io.IOException
     */
    @Test
    public void testSetListaVelocidades() throws IOException {
        out.println("setListaVelocidades");
        LoadModel instance = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));

        List<Pair<Double>> listaVelocidades = new ArrayList<>();
        listaVelocidades.add(new PairDouble(2.0, 3.0));
        listaVelocidades.add(new PairDouble(-2.0, -3.0));

        instance.setListaVelocidades(listaVelocidades);
        
        assertEquals(listaVelocidades, instance.getListaVelocidades());
    }

    /**
     * Test of getMarco method, of class LoadModel.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testGetMarco() throws IOException {
        out.println("getMarco");
        LoadModel instance = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));
        List<Pair<Double>> expResult = new ArrayList<>();
        expResult.add(new PairDouble(-10.0, 10.0));
        expResult.add(new PairDouble(-10.0, 10.0));

        List<Pair<Double>> result = instance.getMarco();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMarco method, of class LoadModel.
     * @throws java.io.IOException
     */
    @Test
    public void testSetMarco() throws IOException {
        out.println("setMarco");
        LoadModel instance = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));
        List<Pair<Double>> marco = new ArrayList<>();
        marco.add(new PairDouble(-5, 5));
        marco.add(new PairDouble(-5, 5));
        
        assertNotEquals(marco, instance.getMarco());
        instance.setMarco(marco);
        assertEquals(marco, instance.getMarco());
    }

    /**
     * Test of getFramesPerSecond method, of class LoadModel.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testGetFramesPerSecond() throws IOException {
        out.println("getFramesPerSecond");
        LoadModel instance = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));
        int expResult = 100;
        int result = instance.getFramesPerSecond();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFramesPerSecond method, of class LoadModel.
     * @throws java.io.IOException
     */
    @Test
    public void testSetFramesPerSecond() throws IOException {
        out.println("setFramesPerSecond");
        LoadModel instance = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));
        int framesPerSecond = 20;
        instance.setFramesPerSecond(framesPerSecond);
        assertEquals(framesPerSecond, instance.getFramesPerSecond());
    }

    /**
     * Test of getTinicial method, of class LoadModel.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testGetTinicial() throws IOException {
        out.println("getTinicial");
        LoadModel instance = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));
        double expResult = 0.2;
        double result = instance.getTinicial();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setTinicial method, of class LoadModel.
     * @throws java.io.IOException
     */
    @Test
    public void testSetTinicial() throws IOException {
        out.println("setTinicial");
        double tinicial = 0.0;
        LoadModel instance = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));
        instance.setTinicial(tinicial);
        double result = instance.getTinicial();
        assertEquals(tinicial, result, 0.0);
    }

    /**
     * Test of getTfinal method, of class LoadModel.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testGetTfinal() throws IOException {
        out.println("getTfinal");
        LoadModel instance = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));
        double expResult = 1.0;
        double result = instance.getTfinal();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setTfinal method, of class LoadModel.
     * @throws java.io.IOException
     */
    @Test
    public void testSetTfinal() throws IOException {
        out.println("setTfinal");
        LoadModel instance = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));
        double tfinal = 1.5;
        instance.setTfinal(tfinal);
        assertEquals(tfinal, instance.getTfinal(), 0.0);
    }

    /**
     * Test of updatePositions method, of class LoadModel.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testUpdatePositions() throws IOException {
        out.println("updatePositions");
        double time = 0.01;
        LoadModel instance0 = new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())));
        instance0.updatePositions(time);
        // se debe agregar validaciÃ³n
        assertTrue(!instance0.equals(new LoadModel(readAllLines(get(this.createdFile.getAbsolutePath())))));
    }

}
