/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.showmodel.loader;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class ListObjectsTest {
    private List<List<Point2D>> listaPuntosObjetos;
    private List<Pair<Double>> listaVelocidades;
    
    /**
     *
     */
    public ListObjectsTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        this.listaPuntosObjetos = new ArrayList<>();
        this.listaVelocidades = new ArrayList<>();
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        this.listaPuntosObjetos.clear();
        this.listaVelocidades.clear();
    }

    /**
     * Test of getUpdatedObjects method, of class ListObjects.
     */
    @Test
    public void testGetUpdatedObjects() {
        out.println("getUpdatedObjects");
        this.listaPuntosObjetos = new ArrayList<>();
        
        List<Point2D> obj1 = new ArrayList<>();
        obj1.add(new Point2D(0, -1));
        obj1.add(new Point2D(3, 2));
        obj1.add(new Point2D(0, 5));
        obj1.add(new Point2D(-3, 2));
        
        List<Point2D> obj2 = new ArrayList<>();
        obj2.add(new Point2D(0, -5));
        obj2.add(new Point2D(3, -2));
        obj2.add(new Point2D(0, 1));
        obj2.add(new Point2D(-3, -2));

        this.listaPuntosObjetos.add(obj1);
        this.listaPuntosObjetos.add(obj2);
        
        this.listaVelocidades = new ArrayList<>();        
        this.listaVelocidades.add(new PairDouble(1, 0));
        this.listaVelocidades.add(new PairDouble(0, -1));

        ListObjects instance = new ListObjects(this.listaPuntosObjetos, this.listaVelocidades);
        
        List<List<Point2D>> expResult = new ArrayList<>();
        List<Point2D> obj1Nuevo = new ArrayList<>();
        obj1Nuevo.add(new Point2D(0.2, -1));
        obj1Nuevo.add(new Point2D(3.2, 2));
        obj1Nuevo.add(new Point2D(0.2, 5));
        obj1Nuevo.add(new Point2D(-2.8, 2));
        expResult.add(obj1Nuevo);

        List<Point2D> obj2Nuevo = new ArrayList<>();
        obj2Nuevo.add(new Point2D(0, -5.2));
        obj2Nuevo.add(new Point2D(3, -2.2));
        obj2Nuevo.add(new Point2D(0, 0.8));
        obj2Nuevo.add(new Point2D(-3, -2.2));
        expResult.add(obj2Nuevo);
        
        double time = 0.2;
        List<List<Point2D>> result = instance.getUpdatedObjects(time);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaObjetos method, of class ListObjects.
     */
    @Test
    public void testGetListaObjetos() {
        out.println("getListaObjetos");
        this.listaPuntosObjetos = new ArrayList<>();
        List<List<Point2D>> expResult = new ArrayList<>();
        
        List<Point2D> obj1 = new ArrayList<>();
        obj1.add(new Point2D(0, -1));
        obj1.add(new Point2D(3, 2));
        obj1.add(new Point2D(0, 5));
        obj1.add(new Point2D(-3, 2));
        expResult.add(new ArrayList<>(obj1));
        
        List<Point2D> obj2 = new ArrayList<>();
        obj2.add(new Point2D(0, -5));
        obj2.add(new Point2D(3, -2));
        obj2.add(new Point2D(0, 1));
        obj2.add(new Point2D(-3, -2));
        expResult.add(new ArrayList<>(obj2));
        
        this.listaPuntosObjetos.add(obj1);
        this.listaPuntosObjetos.add(obj2);
        
        ListObjects instance = new ListObjects(this.listaPuntosObjetos, null);        
        assertEquals(expResult, instance.getListaObjetos());
    }

    /**
     * Test of setListaObjetos method, of class ListObjects.
     */
    @Test
    public void testSetListaObjetos() {
        out.println("setListaObjetos");
        this.listaPuntosObjetos = new ArrayList<>();
        List<List<Point2D>> expResult = new ArrayList<>();
        
        List<Point2D> obj1 = new ArrayList<>();
        obj1.add(new Point2D(0, -1));
        obj1.add(new Point2D(3, 2));
        obj1.add(new Point2D(0, 5));
        obj1.add(new Point2D(-3, 2));
        expResult.add(new ArrayList<>(obj1));
        
        List<Point2D> obj2 = new ArrayList<>();
        obj2.add(new Point2D(0, -5));
        obj2.add(new Point2D(3, -2));
        obj2.add(new Point2D(0, 1));
        obj2.add(new Point2D(-3, -2));
        expResult.add(new ArrayList<>(obj2));
        
        this.listaPuntosObjetos.add(obj1);
        this.listaPuntosObjetos.add(obj2);
        
        ListObjects instance = new ListObjects(null, null);
        instance.setListaObjetos(this.listaPuntosObjetos);
        
        assertEquals(expResult, instance.getListaObjetos());
    }

    /**
     * Test of getListaVelocidades method, of class ListObjects.
     */
    @Test
    public void testGetListaVelocidades() {
        out.println("getListaVelocidades");
        this.listaVelocidades = new ArrayList<>();
        List<PairDouble> expResult = new ArrayList<>();
        
        this.listaVelocidades.add(new PairDouble(1, 0));
        this.listaVelocidades.add(new PairDouble(0, -1));
        this.listaVelocidades.add(new PairDouble(-1, 0));
        this.listaVelocidades.add(new PairDouble(0, 1));
        
        expResult.add(new PairDouble(1, 0));
        expResult.add(new PairDouble(0, -1));
        expResult.add(new PairDouble(-1, 0));
        expResult.add(new PairDouble(0, 1));
        
        ListObjects instance = new ListObjects(null, this.listaVelocidades);
        assertEquals(expResult, instance.getListaVelocidades());
    }

    /**
     * Test of setListaVelocidades method, of class ListObjects.
     */
    @Test
    public void testSetListaVelocidades() {
        out.println("setListaVelocidades");
        this.listaVelocidades = new ArrayList<>();
        List<PairDouble> expResult = new ArrayList<>();
        
        this.listaVelocidades.add(new PairDouble(1, 0));
        this.listaVelocidades.add(new PairDouble(0, -1));
        this.listaVelocidades.add(new PairDouble(-1, 0));
        this.listaVelocidades.add(new PairDouble(0, 1));
        
        expResult.add(new PairDouble(1, 0));
        expResult.add(new PairDouble(0, -1));
        expResult.add(new PairDouble(-1, 0));
        expResult.add(new PairDouble(0, 1));
        
        ListObjects instance = new ListObjects(null, null);
        instance.setListaVelocidades(this.listaVelocidades);
        assertEquals(expResult, instance.getListaVelocidades());
    }    

    /**
     * Test of hashCode method, of class ListObjects.
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        this.listaPuntosObjetos = new ArrayList<>();
        
        List<Point2D> obj1 = new ArrayList<>();
        obj1.add(new Point2D(0, -1));
        obj1.add(new Point2D(3, 2));
        obj1.add(new Point2D(0, 5));
        obj1.add(new Point2D(-3, 2));
        
        List<Point2D> obj2 = new ArrayList<>();
        obj2.add(new Point2D(0, -5));
        obj2.add(new Point2D(3, -2));
        obj2.add(new Point2D(0, 1));
        obj2.add(new Point2D(-3, -2));
        
        this.listaPuntosObjetos.add(obj1);
        this.listaPuntosObjetos.add(obj2);

        this.listaVelocidades = new ArrayList<>();        
        this.listaVelocidades.add(new PairDouble(1, 0));
        this.listaVelocidades.add(new PairDouble(0, -1));
        this.listaVelocidades.add(new PairDouble(-1, 0));
        this.listaVelocidades.add(new PairDouble(0, 1));        
        
        ListObjects instance = new ListObjects(this.listaPuntosObjetos, this.listaVelocidades);
        int expResult = 0;
        int result = instance.hashCode();
        assertNotEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ListObjects.
     */
    @Test
    public void testEquals() {
        out.println("equals");
        this.listaPuntosObjetos = new ArrayList<>();
        
        List<Point2D> obj1 = new ArrayList<>();        
        obj1.add(new Point2D(0, -1));
        obj1.add(new Point2D(3, 2));
        obj1.add(new Point2D(0, 5));
        obj1.add(new Point2D(-3, 2));
        
        List<Point2D> obj2 = new ArrayList<>();
        obj2.add(new Point2D(0, -5));
        obj2.add(new Point2D(3, -2));
        obj2.add(new Point2D(0, 1));
        obj2.add(new Point2D(-3, -2));
        
        this.listaPuntosObjetos.add(obj1);
        this.listaPuntosObjetos.add(obj2);

        this.listaVelocidades = new ArrayList<>();        
        this.listaVelocidades.add(new PairDouble(1, 0));
        this.listaVelocidades.add(new PairDouble(0, -1));
        this.listaVelocidades.add(new PairDouble(-1, 0));
        this.listaVelocidades.add(new PairDouble(0, 1));
        
        ListObjects instance0 = new ListObjects(this.listaPuntosObjetos, this.listaVelocidades);        
        
        // para comparar
        List<List<Point2D>> listaPuntosObjetosTemp = new ArrayList<>();
        List<Pair<Double>> listaVelocidadesTemp = new ArrayList<>();
        
        List<Point2D> obj1Temp = new ArrayList<>();        
        obj1Temp.add(new Point2D(0, -1));
        obj1Temp.add(new Point2D(3, 2));
        obj1Temp.add(new Point2D(0, 5));
        obj1Temp.add(new Point2D(-3, 2));
        
        List<Point2D> obj2Temp = new ArrayList<>();
        obj2Temp.add(new Point2D(0, -5));
        obj2Temp.add(new Point2D(3, -2));
        obj2Temp.add(new Point2D(0, 1));
        
        listaPuntosObjetosTemp.add(obj1Temp);
        listaPuntosObjetosTemp.add(obj2Temp);

        listaVelocidadesTemp.add(new PairDouble(1, 0));
        listaVelocidadesTemp.add(new PairDouble(0, -1));
        listaVelocidadesTemp.add(new PairDouble(-1, 0));
        listaVelocidadesTemp.add(new PairDouble(0, 1));
        
        ListObjects instance1 = new ListObjects(listaPuntosObjetosTemp, listaVelocidadesTemp);

        boolean expResult = false;
        boolean result = instance0.equals(instance1);
        assertEquals(expResult, result);
    }
}